---
title: "Frequently Asked Questions"
date: 2020-01-07T16:09:45-04:00
type: "faq"
layout: "section"
hide_sidebar: true
---
Last Updated: July 18, 2023

## Table of contents

- [What is Sparkplug?](#what-is-sparkplug)
- [Where can I find the Sparkplug specification?](#where-can-i-find-the-sparkplug-specification)
- [Where can I learn more about the Sparkplug specification?](#where-can-i-learn-more-about-the-sparkplug-specification)
- [Does the Sparkplug specification change the MQTT specification?](#does-the-sparkplug-specification-change-the-mqtt-specification)
- [Why should I consider Sparkplug for my IIoT implementations?](#why-should-i-consider-sparkplug-for-my-iiot-implementations)
- [What is a good way to think about what Sparkplug accomplishes?](#what-is-a-good-way-to-think-about-what-sparkplug-accomplishes)
- [How does Sparkplug compare to specifications like OPC-UA?](#how-does-sparkplug-compare-to-specifications-like-opc-ua)
- [What are the key Sparkplug Working Group projects?](#what-are-the-key-sparkplug-working-group-projects)
- [I want to add Sparkplug support to my commercial product or open source project. What are the licensing terms?](#i-want-to-add-sparkplug-support-to-my-commercial-product-or-open-source-project-what-are-the-licensing-terms)
- [Where can I find products that are compatible with Sparkplug?](#where-can-i-find-products-that-are-compatible-with-sparkplug)
- [I understand Cirrus Link Solutions owns a patent related to Sparkplug. Is it safe to implement Sparkplug from a legal standpoint?](#i-understand-cirrus-link-solutions-owns-a-patent-related-to-sparkplug-is-it-safe-to-implement-sparkplug-from-a-legal-standpoint)
- [Are there other Eclipse specification projects operating under the same Eclipse Intellectual Property Policy as Sparkplug?](#other-eclipse-specification-projects-under-same-eclipse-intellectual-property-policy-as-sparkplug)
- [What is the "Sparkplug Compatible" program?](#what-is-the-sparkplug-compatible-program)
- [How can interested developers and organizations get involved?](#how-can-interested-developers-and-organizations-get-involved)

---

## What is Sparkplug? {.h4}

Sparkplug is an open software specification that provides MQTT clients the
framework to seamlessly integrate data from their applications, sensors,
devices, and gateways within the MQTT Infrastructure. It is specifically
designed for use in Industrial Internet of Things (IIoT) architectures to
ensure a high level of reliability and interoperability.

---

## Where can I find the Sparkplug specification? {.h4}

[Download the latest version](/specification/version/3.0/documents/sparkplug-specification-3.0.0.pdf) 
of the Sparkplug specification.

You can find Sparkplug's version history [on this page](/specification/).

---

## Where can I learn more about the Sparkplug specification? {.h4}

The [resources page](https://sparkplug.eclipse.org/resources/) of the Sparkplug
website is a good place to start.

HiveMQ, a member of the working group, published a [comprehensive blog series](https://www.hivemq.com/mqtt-sparkplug-essentials/) 
on Sparkplug.

---

## Does the Sparkplug specification change the MQTT specification? {.h4}

No. Sparkplug is simply a specification that defines how best to use MQTT in
real-time, mission-critical industrial infrastructures. Sparkplug addresses the
following components within an MQTT infrastructure:

1. Sparkplug defines an OT-centric Topic Namespace.
2. Sparkplug defines an OT-centric Payload definition optimized for industrial process variables.
3. Sparkplug defines MQTT Session State management required by real-time OT SCADA systems.

---

## Why should I consider Sparkplug for my IIoT implementations? {.h4}

MQTT has emerged as a dominant messaging transport across the IT and OT
sectors. By design, the MQTT specification does not dictate a Topic Namespace
or any payload encoding. But as IoT is implemented by device OEMs in the
industrial sector, having a well-defined Topic Namespace and payload encoding
enables greater interoperability and a better experience for the end customer.

---

## What is a good way to think about what Sparkplug accomplishes? {.h4}

The Internet of People exploded because of two open technologies: First, HTTP
defined a data exchange protocol. Then there was HTML, which was used to
"define" the data sent by HTTP. Together, HTTP and HTML, these technologies
provided the basis for the explosion of the Internet of People.

We must take the same approach for the IIoT to obtain the same adoption and
growth rate. The widely adopted MQTT message transport plays the role of HTTP,
providing an open and interoperable messaging framework. But the IIoT is
missing the “definition” of the data in the payload. Therefore Sparkplug can be
thought of as the HTML of IIoT.

---

## How does Sparkplug compare to specifications like OPC-UA? {.h4}

Sparkplug infrastructure will not replace OPC but will rather complement it. In
existing brownfield industrial sites, OPC-UA polling engines are used to gain
access to the raw process variables, for example. That said, Sparkplug is a
better choice than OPC-UA in many scenarios. Here are a few points:

- Sparkplug provides pure device-to-OT publish/subscribe Message-oriented
  middleware (MOM) architectures.
- MQTT brokers and Sparkplug are lightweight enough for sensor/device telemetry
  communications.
- Sparkplug is light enough to implement on all types of edge devices.

Moreover, the [Eclipse IoT working group](https://iot.eclipse.org/) is home to
a variety of protocol implementations, such as [Eclipse Californium (CoAP)](https://www.eclipse.org/californium/),
[Eclipse Cyclone DDS (DDS)](https://github.com/eclipse-cyclonedds/cyclonedds), [Eclipse Milo (OPC-UA)](https://github.com/eclipse/milo), 
and [Eclipse Paho (MQTT)](https://www.eclipse.org/paho/), among others. We
believe that, ultimately, it is up to developers to pick the right tool for
their projects.

---

## What are the key Sparkplug Working Group projects? {.h4}

The working group hosts the Sparkplug specification project along with 
[Eclipse Tahu](https://github.com/eclipse/tahu), an existing open source
compatible implementation of the specification. Sparkplug was contributed to
the Eclipse Foundation in 2019 by Cirrus Link Solutions.

---

## I want to add Sparkplug support to my commercial product or open source project. What are the licensing terms? {.h4}

Anyone can implement the Sparkplug specification for any purpose without fee or
royalty under the terms of the [Eclipse Foundation Specification License](https://www.eclipse.org/legal/efsl.php) 
(EFSL). As per that license,  attribution must be provided in any software,
documents, or other items or products you create pursuant to the implementation
of the contents of the Sparkplug specification. In addition, anyone can freely
use the Technology Compatibility Kit (TCK) to test that their implementation of
Sparkplug is compatible with the specification. To have your implementation
listed as compatible and/or to use the Sparkplug Compatible logo with your
product, read more on our [Get Listed](/compatibility/get-listed/) page.

Sparkplug®, Sparkplug Compatible, and the Sparkplug Logo are trademarks of the
Eclipse Foundation.

---

## Where can I find products that are compatible with Sparkplug? {.h4}

You can find the official list of [Sparkplug Compatible products](/compatibility/compatible-software/) 
on the working group's website. Each product listed has been validated to pass
the [Sparkplug Technology Compatibility Kit](https://github.com/eclipse-sparkplug/sparkplug/tree/master/tck) 
(TCK) and is thus fully compliant with the specification.

---

## I understand Cirrus Link Solutions owns a patent related to Sparkplug. Is it safe to implement Sparkplug from a legal standpoint? {.h4}

Yes, it is. Cirrus Link Solutions, a member of the Eclipse Foundation and the
Sparkplug working group, owns a patent related to the Sparkplug specification.
The patent, titled Sparkplug-aware MQTT Server, was filed with the United
States Patent and Trademark Office and was granted document ID US 11121930 B1.

The Sparkplug working group operates under the [Eclipse Intellectual Property Policy](https://www.eclipse.org/org/documents/Eclipse_IP_Policy.pdf). 
Under that policy, Cirrus Link granted an irrevocable (subject to a defensive
termination provision), non-exclusive, worldwide, royalty-free, transferable
patent license with respect to the final specification. This applies to anyone
that makes, uses, sells, offers to sell, and imports Sparkplug implementations,
so long as such implementations successfully pass the corresponding Sparkplug
Technology Compatibility Kit (TCK) and remain in compliance with the 
[Eclipse Foundation TCK License](https://www.eclipse.org/legal/tck.php). 
This also allows customers to purchase their Sparkplug solution from any vendor
whose solution has successfully passed the corresponding Sparkplug Technology
Compatibility Kit (TCK) and who remain in compliance with the Eclipse
Foundation TCK License.

---

## Are there other Eclipse specification projects operating under the same [Eclipse Intellectual Property Policy](https://www.eclipse.org/org/documents/Eclipse_IP_Policy.pdf) as Sparkplug? {#other-eclipse-specification-projects-under-same-eclipse-intellectual-property-policy-as-sparkplug .h4 }

One example is [Jakarta EE](https://jakarta.ee/), the successor to Java
Enterprise Edition, which is a comprehensive set of specifications that have
been maintained under the [Eclipse Intellectual Property Policy](https://www.eclipse.org/org/documents/Eclipse_IP_Policy.pdf) 
since 2018.

Members of the Jakarta EE working group include Fujitsu, IBM, Microsoft,
Oracle, and Red Hat, among others. All these organizations own significant
patent portfolios, and many of those patents apply directly to Jakarta EE.

Since its inception in 2018, we are not aware of any legal action in relation
to patents against holders of Jakarta EE implementations that pass the Jakarta
EE Technology Compatibility Kit (TCK).

---

## What is the "Sparkplug Compatible" program? {.h4}

The Sparkplug Compatible program aims to provide integrators and end-users with
an easy way to procure devices and software products that are fully compatible
with the Sparkplug specification.

If you are a technology provider, getting your products listed showcases a
commitment to Sparkplug while making you a participant in a thriving
marketplace. By joining the compatibility program, you will gain the confidence
of current and future Sparkplug adopters by demonstrating that your product
meets stringent quality and interoperability requirements. In turn, this helps
build your brand and expand your market reach.

The steps for joining the program are described on the [Get Listed](https://sparkplug.eclipse.org/compatibility/get-listed/) 
page of the Sparkplug website.

Participation in the Sparkplug Compatible program is reserved for members of
the Sparkplug Working Group.

If you have any questions, please email <sparkplug@eclipse.org>.

---

## How can interested developers and organizations get involved? {.h4}

Anyone can implement the Sparkplug specification for any purpose without fee or
royalty. You can also use the Technology Compatibility Kit (TCK) to validate
your implementation free of charge.

The Sparkplug specification and TCK are developed in the open on GitHub. You
can find the repository here: https://github.com/eclipse-sparkplug/sparkplug

Anyone can propose changes to the specification or TCK by opening an issue on
the repository or through a pull request. You must sign the electronic 
[Eclipse Contributor Agreement](https://www.eclipse.org/legal/ECA.php) (ECA) to
authorize us to use your contributions.

You can also contribute code to the [Eclipse Tahu project](https://projects.eclipse.org/projects/iot.tahu), 
an open source implementation of Sparkplug. Tahu contains C, C#, Java,
JavaScript, and Python libraries. The Tahu repository is available here:
https://github.com/eclipse/tahu

If you wish to influence the future of Sparkplug directly or participate in the
Sparkplug Compatible program, we recommend joining the working group as a
member. All working group members have the right to designate a committer of
the specification project. Moreover, strategic members get a guaranteed seat on
the Sparkplug specification committee, which ratifies new specification
versions. Email us at <sparkplug@eclipse.org> for more information.

---

{{< grid/div class="margin-bottom-20" isMarkdown="false" >}}
  <small>
    <em>The contents of this page do not constitute legal advice and are provided for informational purposes only.</em>
  </small>
{{</ grid/div >}}
