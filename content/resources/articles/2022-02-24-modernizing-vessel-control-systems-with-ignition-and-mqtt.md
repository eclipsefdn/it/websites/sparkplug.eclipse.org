+++
date = "2022-02-24"
title = "Modernizing Vessel Control Systems with Ignition and MQTT"
link = "https://cirrus-link.com/modernizing-vessel-control-systems-with-ignition-and-mqtt/"
+++

A multinational energy corporation is working to adopt technology to increase efficiency and improve data-driven decision making throughout its fleet of vessels. The vessels are equipped with on board systems including navigation, cargo management, vibration monitoring, engine monitoring and more, and the company set out to make better use of data and workflows in the remote, maritime environment.
