+++
date = "2022-08-25"
title = "MQTT Sparkplug to AWS IoT Sitewise Integration: A Step-By-Step Guide"
link = "https://www.hivemq.com/blog/industrial-data-integration-mqtt-sparkplug-aws-iot-sitewise/"
+++

In this article, I'm going to show you, step-by-step, how to integrate industrial data into AWS IoT Sitewise using MQTT Sparkplug. For demonstartion, I am using Opto 22 Groov RIO as my data source, with my temperature probe representing the temperature of a Wind Turbine system, and my 0-10 volts signal generator representing Wind Turbine speed.
