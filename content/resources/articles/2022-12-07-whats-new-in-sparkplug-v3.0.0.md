+++
date = "2022-12-07"
title = "What’s New in Sparkplug® v3.0.0?"
link = "https://www.hivemq.com/blog/whats-new-eclipse-sparkplug-30-specification-iiot-industry-40/"
+++

The Eclipse Sparkplug Working Group has released the new Sparkplug Specification Version 3.0, upgrading from the v2.2 of the Sparkplug Specification of 2019.
