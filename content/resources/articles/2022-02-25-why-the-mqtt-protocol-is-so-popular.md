+++
date = "2022-02-25"
title = "Why the MQTT Protocol is So Popular?"
link = "https://www.automationworld.com/factory/iiot/article/22080946/why-the-mqtt-protocol-is-so-popular"
+++

This first installment in a four-part series on key industrial network technologies explains how an ultra lightweight data transfer protocol became a widely used data gathering tool for Internet of Things applications.
