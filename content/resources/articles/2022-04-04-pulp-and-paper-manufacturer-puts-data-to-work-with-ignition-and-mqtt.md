+++
date = "2022-04-04"
title = "Pulp and Paper Manufacturer Puts Data to Work with Ignition and MQTT"
link = "https://cirrus-link.com/pulp-and-paper-manufacturer-puts-data-to-work-with-ignition-and-mqtt/"
+++

A large pulp and paper company set out to digitally transform over 50 manufacturing plants across the United States. They wanted to modernize an outdated SCADA system to improve data mobility, data standardization, and data utilization for improved operations.
