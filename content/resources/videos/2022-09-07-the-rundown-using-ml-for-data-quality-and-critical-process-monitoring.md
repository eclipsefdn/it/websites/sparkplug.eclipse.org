+++
date = "2022-09-07"
title = "The RUNDOWN - Using ML for data quality and critical process monitoring"
link = "https://www.linkedin.com/video/event/urn:li:ugcPost:6971139518422818816/"
link_class  = "eclipsefdn-video"
src_site = "others"
video_img = "/images/thumbnails/2022-09-07.jpg"
tags = [ "video", "machine learning", "eclipse", "sparkplug", "process monitoring", "critical", "data quality"]
categories = ["video"]
+++

Join Aperio, Opto 22, Canary, and HiveMQ as we discuss options around how to use machine learning for validating process data and alerting on critical operational issues normally overlooked.
