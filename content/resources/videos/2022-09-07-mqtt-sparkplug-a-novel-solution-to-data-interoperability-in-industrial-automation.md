+++
date = "2022-09-07"
title = "MQTT Sparkplug: A Novel Solution to Data Interoperability in Industrial Automation"
link = "https://www.youtube.com/watch?v=WpoVnNc5_yA"
link_class  = "eclipsefdn-video"
tags = [ "video", "open source", "eclipse", "sparkplug", "mqtt", "data interoperability", "industrial automation"]
categories = ["video"]
+++

At the heart of any successful Industrial IoT(IIoT) project lies seamless bidirectional data flow and end-to-end data interoperability. In practice, however, it is quite challenging to get the OT and the IT systems talking to each other and to the cloud and the edge. A novel solution to bringing end-to-end IIoT data interoperability and exchange is adopting the MQTT Sparkplug specification.

Follow along Dominik Obermaier, CTO and Co-founder of HiveMQ, as he discusses how MQTT Sparkplug can be a gamechanger for your industrial automation and digital transformation efforts.