+++
date = "2022-04-01"
title = "Modernize Your Industry 4.0 Architecture with UNS, ISA95, and MQTT Sparkplug"
link = "https://www.youtube.com/watch?v=iAzThMOTvR0"
link_class  = "eclipsefdn-video"
tags = [ "video", "open source", "Eclipse", "sparkplug", "architecture", "MQTT", "UNS", "ISA95"]
categories = ["video"]
+++

The concept of integrating an entire organization’s value chain to the point of achieving lot-size-one (LSO) manufacturing capabilities is often the goal of modern Industry 4.0 enterprises in the manufacturing industry.
