---
title: "Sparkplug version 2.2"
author: Cirrus Link Solutions
version_number: "2.2"
version_description: |
    Migration to Eclipse Foundation Branding
version_date: 2019-10-11
specification_pdf: documents/sparkplug-specification-2.2.pdf
type: specification_version
description: ""
categories: []
keywords: []
slug: ""
aliases: []
redirect_url: "../../"
---
